import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  FlatList
} from 'react-native'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'

const SettingsView = styled.View`
  background-color: #fff;
  flex: 1;
`

const Container = styled.View`
  width: 85%;
  margin: 20px auto;
`

const HeadingText = styled.Text`
  font-size: 38;
  font-weight: 800;
  margin-top: 30;
`

const ListItemView = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: space-between;
  height: 60;
  border-bottom-width: 1;
  border-color: #F2F2F2;
`

const ItemText = styled.Text`
  font-size: 21;
  color: ${ props => props.baseColor };
  align-self: center;
`

const InfoText = styled.Text`
  font-size: 15;
  color: #C6C6C6;
  position: absolute;
  bottom: 30;
  text-align: center;
  align-self: center;
`

class SettingsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    mode: 'modal',
    headerMode: 'none',
  })

  render() {
    const { onPress, navigation, colors } = this.props
    return (
      <SettingsView>
        <SafeAreaView/>
        <Container>
          <TouchableOpacity style = {{ marginTop: -20 }}>
            <Ionicons onPress = {() => navigation.goBack()} name = "ios-close" size = {40}/>
          </TouchableOpacity>
          <HeadingText>Settings</HeadingText>
          <ListItemView onPress = {() => navigation.navigate('ChangeColorScreen') }>
            <ItemText baseColor = { colors.baseColor }>Change Color</ItemText>
            <Ionicons color = { colors.baseColor } style = {{ alignSelf: 'center' }} name = "ios-arrow-forward" size = {20} />
          </ListItemView>
          <ListItemView>
            <ItemText baseColor = { colors.baseColor }>Premium Account</ItemText>
            <Ionicons color = { colors.baseColor } style = {{ alignSelf: 'center' }} name = "ios-arrow-forward" size = {20} />
          </ListItemView>
          <ListItemView>
            <ItemText baseColor = { colors.baseColor }>Terms Of Use</ItemText>
            <Ionicons color = { colors.baseColor } style = {{ alignSelf: 'center' }} name = "ios-arrow-forward" size = {20} />
          </ListItemView>
        </Container>
        <InfoText>Powered by Understudios{"\n"}Version 0.1.1</InfoText>
      </SettingsView>
    )
  }
}

const mapStateToProps = state => ({
  colors: state.color.color
})

export default connect(mapStateToProps, null)(SettingsScreen)
