import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  FlatList,
  TouchableWithoutFeedback
} from 'react-native'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import LottieView from 'lottie-react-native'
import LinearGradient from 'react-native-linear-gradient'

import { changeColor } from './../actions/changeColorActions'

const SettingsView = styled.ScrollView`
  background-color: #fff;
  flex: 1;
`

const Container = styled.View`
  width: 85%;
  margin: 20px auto;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`

const HeadingText = styled.Text`
  font-size: 38;
  font-weight: 800;
  margin-left: 7.5%;
`

const InfoText = styled.Text`
  font-size: 15;
  color: #C6C6C6;
  position: absolute;
  bottom: 30;
  text-align: center;
  align-self: center;
`

const ColorBox = styled.TouchableOpacity`
  width: 100%;
  height: 100%;
`

const ColorBoxButton = styled.View`
  width: 29;
  height: 29;
  background: #FFFFFF;
  border-radius: 14.5;
  margin: 15px;
`

const ColorBoxText = styled.Text`
  font-size: 24;
  font-weight: 600;
  color: #FFFFFF;
  position: absolute;
  bottom: 10;
  left: 15;
`

const LottieBox = styled.View`
  width: 105;
  height: 105;
  marginLeft: -23;
  marginTop: -23;
`

class ChangeColorScreen extends Component {
  constructor(props){
    super(props)
  }
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      borderBottomWidth: 0,
      elevation: null,
      backgroundColor: '#FFF',
    },
    headerTintColor: '#000000',
  })

  render() {
    const { navigation, changeColor, user, colors } = this.props
    return (
      <SettingsView>
        <SafeAreaView/>
        {
          user._id === '5c6362ac8497fb15fe714cb6' ||
          user._id === '5c63e9218497fb15fe714cc0' ||
          user._id === '5c6359d38497fb15fe714cb3' ?
          <View>
            <HeadingText>Kamarádský{"\n"}Speciál</HeadingText>
            <Container>
              <ColorBox onPress = { () => changeColor({from: '#8A26BF', to: '#4B0082', name: 'purple', baseColor: '#4B0082'}) } color = 'purple'>
                <ColorBoxButton />
                <ColorBoxText>Ríša</ColorBoxText>
              </ColorBox>
              <ColorBox onPress = { () => changeColor({from: '#FC5696', to: '#D32669', name: 'pink', baseColor: '#FC5696'}) }  color = 'pink'>
                <ColorBoxButton />
                <ColorBoxText>Kubík</ColorBoxText>
              </ColorBox>
              <ColorBox onPress = { () => changeColor({from: '#56B6FC', to: '#269ED3', name: 'lambo', baseColor: '#56B6FC'}) } color = 'lambo'>
                <ColorBoxButton />
                <ColorBoxText>Honzík</ColorBoxText>
              </ColorBox>
              <ColorBox onPress = { () => changeColor({from: '#262626', to: '#000000', name: 'black', baseColor: '#000000'}) } color = 'black'>
                <ColorBoxButton />
                <ColorBoxText>Dark</ColorBoxText>
              </ColorBox>
            </Container>
          </View>
          :
          <View>
            <HeadingText>Change{"\n"}Color</HeadingText>
            <Container>
              <LinearGradient colors={['#4D62FC', '#263AD3']} style = {{
                  height: 200,
                  width: '48.5%',
                  borderRadius: 18,
                  marginTop: '2%'
                  }}>
                <ColorBox onPress = { () => changeColor({from: '#4D62FC', to: '#263AD3', name: 'blue', baseColor: '#263AD3'}) }>
                  {
                    colors.baseColor === '#263AD3' ?
                    <LottieBox>
                      <LottieView
                        source = { require('./../animations/done.json') }
                        autoPlay
                        loop = { false }/>
                    </LottieBox>
                    :
                    <ColorBoxButton />
                  }
                  <ColorBoxText>Blue</ColorBoxText>
                </ColorBox>
              </LinearGradient>
              <LinearGradient colors={['#F26060', '#B42525']} style = {{
                  height: 200,
                  width: '48.5%',
                  borderRadius: 18,
                  marginTop: '2%'
                  }}>
                <ColorBox onPress = { () => changeColor({from: '#F26060', to: '#B42525', name: 'red', baseColor: '#D14141'}) }>
                  {
                    colors.baseColor === '#D14141' ?
                    <LottieBox>
                      <LottieView
                        source = { require('./../animations/done.json') }
                        autoPlay
                        loop = { false }/>
                    </LottieBox>
                    :
                    <ColorBoxButton />
                  }
                  <ColorBoxText>Red</ColorBoxText>
                </ColorBox>
              </LinearGradient>
              <LinearGradient colors={['#6BC483', '#3E8E54']} style = {{
                  height: 200,
                  width: '48.5%',
                  borderRadius: 18,
                  marginTop: '2%'
                  }}>
                <ColorBox onPress = { () => changeColor({from: '#6BC483', to: '#3E8E54', name: 'green', baseColor: '#55AA6C'}) }>
                  {
                    colors.baseColor === '#55AA6C' ?
                    <LottieBox>
                      <LottieView
                        source = { require('./../animations/done.json') }
                        autoPlay
                        loop = { false }/>
                    </LottieBox>
                    :
                    <ColorBoxButton />
                  }
                  <ColorBoxText>Green</ColorBoxText>
                </ColorBox>
              </LinearGradient>
              <LinearGradient colors={['#EAE067', '#C9C04F']} style = {{
                  height: 200,
                  width: '48.5%',
                  borderRadius: 18,
                  marginTop: '2%'
                  }}>
                <ColorBox onPress = { () => changeColor({from: '#EAE067', to: '#C9C04F', name: 'yellow', baseColor: '#DBD15C'}) }>
                  {
                    colors.baseColor === '#DBD15C' ?
                    <LottieBox>
                      <LottieView
                        source = { require('./../animations/done.json') }
                        autoPlay
                        loop = { false }/>
                    </LottieBox>
                    :
                    <ColorBoxButton />
                  }
                  <ColorBoxText>Yellow</ColorBoxText>
                </ColorBox>
              </LinearGradient>
            </Container>
          </View>
        }
      </SettingsView>
    )
  }
}

const mapStateToProps = state => {
  return {
    colors: state.color.color,
    user: state.user.data.user
  }
}

const mapDispatchToProps = dispatch => {
	return {
		changeColor: color => dispatch(changeColor(color))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeColorScreen)
