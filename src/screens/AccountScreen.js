import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  FlatList,
  Image,
  ImageBackground,
  AsyncStorage,
  CameraRoll,
  Modal
} from 'react-native'
import styled from 'styled-components/native'
import { connect } from 'react-redux'
import { SignOut, uploadAvatar } from './../actions/userActions'
import { CachedImage } from 'react-native-cached-image'
import Ionicons from 'react-native-vector-icons/Ionicons'
import ImagePicker from 'react-native-image-picker';
import { getCurrentUser } from './../actions/userActions'

import TestImage from './../images/profile-test.jpg'
import AddButton from './../components/AddButton'
import PhotosView from '../components/PhotosView'
import LottieView from 'lottie-react-native'

const AccountView = styled.View`
  flex: 1;
  overflow: hidden;
`

const BackgroundView = styled.View`
  width: 200%;
  height: 500;
  position: absolute;
  left: -50%;
  top: -200;
  z-index: -999;
  transform: rotate(-15deg);
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
  overflow: hidden;
`

const Container = styled.View`
  width: 85%;
  margin: 20px auto;
`

const NameAndIconView = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 40%;
`

const YourNameText = styled.Text`
  font-size: 38;
  font-weight: 800;
  color: #FFFFFF;
  align-self: flex-start;
  text-align: left;
  flex-wrap: wrap;
  max-width: 200;
`

const IconView = styled.TouchableOpacity`
  width: 100;
  height: 100;
  border-radius: 50;
  background: #FFFFFF;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
  justify-content: center;
  align-items: center;
`

const List = styled.View`
  padding-top: 40;
`

const ListItemView = styled.View`
  width: 100%;
  height: 70;
  border-bottom-width: 1px;
  border-color: #F2F2F2;
  margin-top: 10;
`

const ItemKey = styled.Text`
  font-size: 21;
  color: ${props => props.baseColor};
  font-weight: 500;
`

const ItemText = styled.Text`
  font-size: 24;
  font-weight: 700;
  margin-top: 10;
`

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class AccountScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      avatar: {}
    }
  }

  componentDidMount(){
    this.props.getCurrentUser()
  }

  uploadAvatar = async () => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.uploadPhoto(response)
      }
    });
  }

  uploadPhoto = async (avatar) => {
    this.props.uploadAvatar(avatar,this.props.user.user._id)
  }

  render() {
    const { onPress, user, colors, loading } = this.props
    let imageUri;
    if (user.user.avatar) {
      imageUri = { uri: `http://207.154.211.81:1337${user.user.avatar.url}` }
    }else {
      imageUri = TestImage
    }

    return (
      <AccountView>
        <SafeAreaView/>
        <BackgroundView>
          {
            loading ? 
            <LottieView
              source = { require('./../animations/avatar.json') }
              autoplay
              loop
              progress = { this.state.progress }/>
            :
            <CachedImage style = {{
              'width': '100%',
              'height': '120%',
              transform: [{ rotate: '15deg'}],
              'resizeMode': 'cover'
            }} resizeMode = {'cover'} source = {imageUri}/>
          }
        </BackgroundView>
        <Container>
          <TouchableOpacity style = {{ marginTop: -20 }} onPress = {() => onPress()}>
            <Ionicons color = "#FFFFFF" name = "ios-close" size = {40}/>
          </TouchableOpacity>
          <NameAndIconView>
            <YourNameText numberOfLines = {2}>{ user.user.username }</YourNameText>
            <IconView onPress = { () => this.uploadAvatar() }>
              <Ionicons name = "ios-camera" size = { 40 } color = { colors.baseColor } />
            </IconView>
          </NameAndIconView>
        </Container>
        <Container>
          <List>
            <ListItemView>
              <ItemKey baseColor = { colors.baseColor } >Email</ItemKey>
              <ItemText>{user.user.email}</ItemText>
            </ListItemView>
            <ListItemView>
              <ItemKey baseColor = { colors.baseColor } >Premium until</ItemKey>
              <ItemText>30.09.2019</ItemText>
            </ListItemView>
          </List>
        </Container>
        <AddButton baseColor = { colors.baseColor } text = "Sign Out" onPress = { () => this._signOutAsync() } />
      </AccountView>
    )
  }
  _signOutAsync = async () => {
    this.props.signOut()
    this.props.navigation.navigate('Auth');
  };
}

const mapStateToProps = state => ({
  user: state.user.data,
  colors: state.color.color,
  loading: state.user.isLoading
})

const mapDispatchToProps = dispatch => {
	return {
		signOut: () => dispatch(SignOut()),
    uploadAvatar: (avatar,id) => dispatch(uploadAvatar(avatar,id)),
    getCurrentUser: () => dispatch(getCurrentUser())
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(AccountScreen)
