import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Modal
} from 'react-native'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FAIcons from 'react-native-vector-icons/FontAwesome5'
import { connect } from 'react-redux'
import { fetchWorkspaces, acceptInvite } from './../actions/workspaceActions'

import WorkSpace from './../components/WorkSpace'
import PendingWorkSpace from './../components/PendingWorkSpace'
import SharedWorkSpace from './../components/SharedWorkSpace'
import AddButton from './../components/AddButton'
import SettingsScreen from './SettingsScreen'
import AccountScreen from './AccountScreen'

const HomeScreenView = styled.View`
  flex: 1;
`

const TopbarView = styled.View`
  width: 100%;
  flex-direction: row;
  margin-top: 20;
`

const IconWithNameView = styled.TouchableOpacity`
  flex-direction: row;
`

const Container = styled.View`
  flex: 1;
  width: 90%;
  margin: 0 auto;
`

const AccountNameText = styled.Text`
  font-size: 12;
  font-weight: 500;
  color: ${ props => props.baseColor };
  margin-left: 10;
  align-self: center;
`

const SettingsTouchable = styled.TouchableOpacity`
  margin-left: auto;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
`

const LeadingText = styled.Text`
  font-size: 38;
  color: #000;
  margin-top: 15;
  margin-bottom: 10;
  font-weight: 800;
`

const WorkSpacesView = styled.View`
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 100;
`

class HomeScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      modalVisible: false,
      AccountModalVisible: false
    }
  }

  componentDidMount() {
    this.props.fetchWorkspaces()
    this.props.navigation.setParams({
      color: this.props.colors.from
    })
  }

  componentDidUpdate(prevProps){
    if (prevProps.colors !== this.props.colors ) {
      this.props.navigation.setParams({
        color: this.props.colors.from
      })
    }
  }

  refresh = async () => {
    await this.props.fetchWorkspaces()
  }

  render () {
    const { navigation, t, workspaces, user, colors } = this.props
    // const METHOD_DATA = [{
    //   supportedMethods: ['apple-pay'],
    //   data: {
    //     merchantIdentifier: 'merchant.com.your-app.namespace',
    //     supportedNetworks: ['visa', 'mastercard', 'amex'],
    //     countryCode: 'US',
    //     currencyCode: 'USD'
    //   }
    // }];
    // const DETAILS = {
    //   id: 'basic-example',
    //   displayItems: [
    //     {
    //       label: 'Movie Ticket',
    //       amount: { currency: 'USD', value: '5.00' }
    //     }
    //   ],
    //   total: {
    //     label: 'Merchant Name',
    //     amount: { currency: 'USD', value: '5.00' }
    //   }
    // };
    // const paymentRequest = new PaymentRequest(METHOD_DATA, DETAILS);
    //paymentRequest.show();
    return (
      <HomeScreenView>
        <ScrollView>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.AccountModalVisible}>
            <AccountScreen
              navigation = { navigation }
              onPress = { () => this.setState({ AccountModalVisible: !this.state.AccountModalVisible }) }/>
          </Modal>
          <SafeAreaView/>
          <Container>
            <TopbarView>
              <IconWithNameView onPress = { () => this.setState({ AccountModalVisible: !this.state.AccountModalVisible }) }>
                <Ionicons name = "ios-person" color = { colors.baseColor } size = {26} />
                <AccountNameText baseColor = { colors.baseColor } >{user.username}</AccountNameText>
              </IconWithNameView>
              <SettingsTouchable
                onPress={() => {
                  navigation.navigate('SettingsScreen')
                }}>
                <Ionicons name = "ios-settings" color = { colors.baseColor } size = {26} />
              </SettingsTouchable>
            </TopbarView>
            <LeadingText>Your{"\n"}Workspaces.</LeadingText>
            <WorkSpacesView>
              { workspaces &&
                workspaces.map((w,i) => {
                  if ((w.users[0]._id === user._id || w.shared === true) && w.users.length > 1) {
                    return <SharedWorkSpace shared = { w.shared } ownUser = { user } onPress = { () => navigation.navigate('DetailScreen', {workspace: w, headerColor: colors.from}) } key = {i} workspace = {w} baseColor = { colors.baseColor } />
                  }else if (w.users.length > 1 && w.shared === false) {
                    return <PendingWorkSpace ownUser = { user } onAcceptPress = { () => this.props.acceptInvite(w._id)} key = {i} workspace = {w} baseColor = { colors.baseColor } />
                  }else {
                    return <WorkSpace icon = { w.icon } baseColor = { colors.baseColor } onPress = { () => navigation.navigate('DetailScreen', {workspace: w, headerColor: colors.from}) } key = {i} name = { w.name } />
                  }
                })
              }
            </WorkSpacesView>
          </Container>
        </ScrollView>
        <AddButton
          baseColor = { colors.baseColor }
          text = "Add new workspace"
          onPress = { () => navigation.navigate('AddWorkspace', {headerColor: colors.from}) }
          />
      </HomeScreenView>
    )
  }
}

const mapStateToProps = state => {
  return {
    workspaces: state.workspaces.data,
    user: state.user.data.user,
    colors: state.color.color
  }
}

const mapDispatchToProps = dispatch => {
	return {
		fetchWorkspaces: userId => dispatch(fetchWorkspaces(userId)),
    acceptInvite: id => dispatch(acceptInvite(id))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
