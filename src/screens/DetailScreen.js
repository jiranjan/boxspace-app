import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Alert,
  StyleSheet,
} from 'react-native'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'
import AnimatedModal from 'react-native-modal'
import styled from 'styled-components/native'
import { CachedImage } from 'react-native-cached-image'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { addTodo, fetchTodos, changeTodoState } from './../actions/todoActions'
import { addNote, fetchNotes } from './../actions/noteActions'
import { updateWorkspace } from './../actions/workspaceActions'
import LinearGradient from 'react-native-linear-gradient'

import Todos from './../components/Todos'
import Notes from './../components/Notes'
import AddButton from './../components/AddButton'
import BlankImage from './../images/account.png'

const DetailView = styled.ScrollView`
  flex: 1;
  overflow: hidden;
`

const BackgroundView = styled.View`
  width: 200%;
  height: 400;
  position: absolute;
  left: -50%;
  top: -150;
  z-index: -999;
  background: #4D62FC;
  transform: rotate(-15deg);
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
`

const WorkspaceNameText = styled.Text`
  font-weight: 700;
  font-size: 38;
  color: #FFF;
  margin-top: 20;
`

const Container = styled.View`
  width: 85%;
  margin: 0 auto;
`

const PersonsView = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 15;
`

const PersonAvatarsView = styled.View`
  flex-direction: row;
`

const PersonAvatar = styled.View`
  width: 30;
  height: 30;
  border-radius: 15;
  background: #FFF;
  margin-right: 5;
`

const HowManyPersonsText = styled.Text`
  font-size: 12;
  font-weight: 400;
  color: #FFF;
  align-self: center;
  margin-left: 5;
`

const AddPersonView = styled.TouchableOpacity`
  flex-direction: row;
  height: 30;
`

const AddPersonText = styled.Text`
  font-size: 12;
  font-weight: 400;
  color: #FFF;
  align-self: center;
  margin-right: 5;
`

const AddRecipeView = styled.View`
  width: 100%;
  height: ${props => props.note ? 300 : 230};
  background: #fff;
  border-radius: 15;
  marginBottom: ${props => props.note ? 170 : 100};
`

const AddRecipeHeadingText = styled.Text`
  font-size: 29;
  font-weight: 600;
  margin: 30px auto;
`

const AddRecipeInput = styled.TextInput`
  height: 30;
  width: 80%;
  border-bottom-width: 2px;
  border-color: #263AD3;
  padding-left: 5;
  margin: 0 auto;
  margin-top: ${props => props.note ? 40 : 0};
`

const BottombarView = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  position: absolute;
  bottom: 20;
  width: 100%;
  margin: 0 auto;
`

const BottombarButtonText = styled.Text`
  font-size: 17;
  font-weight: 500;
`

const BottombarButtonTouchable = styled.TouchableOpacity`
  align-self: center;
  padding: 15px;
`

const CenterSpacer = styled.View`
  height: 45;
  width: 1;
  background: #707070;
`

class DetailScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      index: 0,
      routes: [
        { key: 'first', title: 'To-Dos' },
        { key: 'second', title: 'Notes' },
      ],
      modalVisible: false,
      name: '',
      content: '',
      workspace: [],
      todos: [],
      notes: [],
      addUser: false,
      userEmail: ''
    }
  }

  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      borderBottomWidth: 0,
      elevation: null,
      backgroundColor: navigation.state.params.headerColor
    },
    headerTintColor: '#FFF',
  })

  componentWillUnmount(){
    this.setState({ workspace: [], todos: [] })
  }

  componentDidUpdate(prevProps,prevState){
    if (prevProps.workspaceError !== this.props.workspaceError) {
      return setTimeout(() => Alert.alert('Given user does not exist.'), 500)
    }
  }

  async componentDidMount(){
    const workspace = await this.props.navigation.getParam('workspace', null)
    const workspaceId = workspace._id
    await this.props.fetchTodos(workspaceId)
    await this.props.fetchNotes(workspaceId)
    this.setState({ workspace })
  }

  updateWorkspace = async (userEmail,workspaceUsers, workspaceId) => {
    console.log('hi');
    this.props.updateWorkspace(userEmail, workspaceUsers, workspaceId)
  }

  add = async () => {
    if (this.state.index === 0) {
      await this.props.addTodo(this.state.name, false, this.state.workspace._id)
    }else {
      await this.props.addNote(this.state.name, this.state.content, this.state.workspace._id, this.props.user.username)
    }
  }

  changeTodoState = (state,id,wID) => {
    this.props.changeTodoState(state,id,wID)
  }

  render(){
    const { navigation, todos, notes, colors, user } = this.props
    const { workspace } = this.state
    let PersonAvatar;
    if (workspace.length !== 0) {
      PersonAvatar = workspace.users.map((u,i) => {
        if (u._id !== user._id) {
          return <CachedImage key = {i} style = {{ width: 30, height: 30, borderRadius: 15 }} source = {u.avatar ? { uri: `http://207.154.211.81:1337${u.avatar.url}` } : BlankImage} />
        }
        return null
      })
    }
    return(
      <View style = {{ flex: 1 }}>
        <DetailView>
          <SafeAreaView/>
          <LinearGradient colors={[colors.from, colors.from, colors.from, colors.from, colors.to]} style={styles.linearGradient} />
            <AnimatedModal
              useNativeDriver = { false }
              animationIn = "fadeInUpBig"
              animationOut = "fadeOutDownBig"
              isVisible = { this.state.modalVisible }
              onSwipe = {() => this.setState({ modalVisible: false })}
              swipeDirection = "down">
              <AddRecipeView keyboardShouldPersistTaps = { 'always' } note = { this.state.index === 1 && this.state.addUser === false ? true : false }>
                <AddRecipeHeadingText>{ this.state.addUser === true ? 'Add Person' : this.state.index === 0 ? 'New Todo' : 'New Note' }</AddRecipeHeadingText>
                {
                  this.state.addUser === false ?
                  <AddRecipeInput
                    value = { this.state.name }
                    onChangeText = { name => this.setState({ name }) }
                    placeholder = { this.state.index === 0 ? 'Todo name' : 'Note name' }
                    autoFocus
                    placeholderTextColor = { '#C6C6C6' }/>
                  :
                  <AddRecipeInput
                    value = { this.state.userEmail }
                    onChangeText = { userEmail => this.setState({ userEmail }) }
                    placeholder = 'Users email'
                    autoFocus
                    autoCapitalize = "none"
                    placeholderTextColor = { '#C6C6C6' }/>
                }
                {
                  this.state.index === 1 && this.state.addUser === false &&
                  <AddRecipeInput
                    note
                    multiline
                    value = { this.state.content }
                    onChangeText = { content => this.setState({ content }) }
                    placeholder = 'Note content'
                    placeholderTextColor = { '#C6C6C6' }/>
                }
                <BottombarView keyboardShouldPersistTaps = { 'always' }>
                  <BottombarButtonTouchable
                    keyboardShouldPersistTaps = { 'always' }
                    onPress = {() => this.setState({ modalVisible: false, addUser: false })}>
                    <BottombarButtonText>Back</BottombarButtonText>
                  </BottombarButtonTouchable>
                  <CenterSpacer/>
                  <BottombarButtonTouchable
                    keyboardShouldPersistTaps = { 'always' }
                    onPress = {() =>
                      this.state.name !== '' && this.state.addUser === false ? this.add().then(() => this.setState({ modalVisible: false }))
                      : this.state.addUser === true && this.state.userEmail !== '' ? this.updateWorkspace(this.state.userEmail, workspace.users, workspace._id).then(() => this.setState({ modalVisible: false, addUser: false }))
                      : null }>
                    <BottombarButtonText>OK</BottombarButtonText>
                  </BottombarButtonTouchable>
                </BottombarView>
              </AddRecipeView>
            </AnimatedModal>
          <Container>
            <WorkspaceNameText>{workspace.name}</WorkspaceNameText>
            <PersonsView>
              <PersonAvatarsView>
                { PersonAvatar }
                <HowManyPersonsText>{ workspace.users !== undefined && workspace.users.length > 1 ? `${workspace.users.length - 1} ${workspace.users.length > 2 ? 'users' : workspace.shared ? 'user' : 'user pending'}` : 'It\'s just you.'}</HowManyPersonsText>
              </PersonAvatarsView>
              <AddPersonView onPress = { () => this.setState({ name: '', addUser: true, modalVisible: true })}>
                <AddPersonText>Add person</AddPersonText>
                <Ionicons name = "ios-add-circle" size = { 33 } color = "#FFFFFF" />
              </AddPersonView>
            </PersonsView>
          </Container>
          <View style = {{
              height: '180%',
              width: '85%',
              alignSelf: 'center',
            }}>
            <TabView
              style = {{
                marginTop: 20,
                width: '118%',
                marginLeft: '-9%'
              }}
              navigationState={this.state}
              renderTabBar={props =>
                <TabBar
                  {...props}
                  indicatorStyle = {{
                    backgroundColor: '#FFF',
                    marginBottom: 10,
                    width: 80,
                    marginLeft: 15
                  }}
                  tabStyle = {{
                    width: 110
                  }}
                  labelStyle = {{
                    fontWeight: '600',
                    fontSize: 16,
                    textTransform: 'capitalize',
                  }}
                  style = {{
                    backgroundColor: 'transparent',
                    marginLeft: '22%'
                  }}
                />
              }
              renderScene = {({ route }) => {
                switch (route.key) {
                  case 'first':
                    return <Todos changeTodoState = { (state,id,wID) => this.changeTodoState(state,id,wID) } todos = { todos } workspaceName = { workspace.name } />
                  case 'second':
                    return <Notes notes = { notes } workspaceName = { workspace.name } />
                  default:
                    return null
                }
              }}
              onIndexChange={index => this.setState({ index })}
            />
          </View>
        </DetailView>
        <AddButton baseColor = { colors.baseColor } text = { this.state.index === 0 ? "Add new Todo" : "Add new Note"} onPress = { () => this.setState({ name: '', content: '', modalVisible: true }) } />
      </View>
    )
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    width: '200%',
    height: 1000,
    position: 'absolute',
    left: '-50%',
    top: -730,
    zIndex: -999,
    transform: [{ rotate: '-15deg'}],
    shadowOpacity: 0.61,
    shadowRadius: 3,
    shadowColor: 'rgba(209,209,209,1)',
    resizeMode: 'cover'
  }
})

const mapStateToProps = (state, ownProps) => {
  const ownWorkspace = ownProps.navigation.getParam('workspace', {})
  return {
    workspaces: state.workspaces.data,
    workspaceError: state.workspaces.error,
    todos: state.todos.data,
    notes: state.notes.data,
    colors: state.color.color,
    user: state.user.data.user
  }
}

const mapDispatchToProps = dispatch => {
	return {
		addTodo: (todoName,done,workspaceId) => dispatch(addTodo(todoName,done,workspaceId)),
    fetchTodos: id => dispatch(fetchTodos(id)),
    addNote: (name,content,id,userName) => dispatch(addNote(name,content,id,userName)),
    fetchNotes: id => dispatch(fetchNotes(id)),
    changeTodoState: (state,id,wID) => dispatch(changeTodoState(state,id,wID)),
    updateWorkspace: (usersEmails,allUsersId,workspaceId) => dispatch(updateWorkspace(usersEmails,allUsersId,workspaceId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailScreen)
