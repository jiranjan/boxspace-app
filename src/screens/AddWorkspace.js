import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  StyleSheet,
  ScrollView
} from 'react-native'
import { SafeAreaView } from 'react-navigation'
import { connect } from 'react-redux'
import { createWorkspace } from './../actions/workspaceActions'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import LinearGradient from 'react-native-linear-gradient'

import AddButton from './../components/AddButton'

const AddWorkspaceView = styled.View`
  flex: 1;
  overflow: hidden;
`

const BackgroundView = styled.View`
  width: 200%;
  height: 100%;
  position: absolute;
  left: -50%;
  top: -60%;
  background: #4D62FC;
  transform: rotate(-15deg);
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
`

const Container = styled.View`
  width: 85%;
  height: 95%;
  margin: auto;
  margin-bottom: 40;
`

const EmailInput = styled.View`
  margin-top: 40;
`

const EmailText = styled.Text`
  font-size: 21;
  font-weight: 500;
  color: #FFFFFF;
`

const Email = styled.TextInput`
  borderBottomWidth: 2px;
  border-color: #FFFFFF;
  margin-top: 20;
  padding-bottom: 5;
  padding-left: 5;
`

const IconsView = styled.View`
  width: 100%;
  height: 200%;
  margin-top: 45;
  background: #FFF;
  border-radius: 18;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
`

const IconsText = styled.Text`
  font-size: 21;
  font-weight: 500;
  margin-top: 50;
  margin-left: 5;
`

const Icons = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-top: 10;
  width: 100%;
`

const IconView = styled.TouchableOpacity`
  width: 80;
  height: 80;
  background: ${props => props.active ? '#EFEFEF' : '#FFF'};
  border-radius: 12;
  align-items: center;
  justify-content: center;
  margin: 5px;
`

class AddWorkspace extends Component {
  constructor(props){
    super(props)
    this.state = {
      name: '',
      activeIconName: '',
      index: null
    }
  }

  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      borderBottomWidth: 0,
      elevation: null,
      backgroundColor: navigation.state.params.headerColor
    },
    headerTintColor: '#FFF',
  })

  _createWorkspace = async name => {
    const users = [
      this.props.userId
    ]
    await this.props.createWorkspace(name,this.state.activeIconName,users)
    this.props.navigation.goBack()
  }

  render(){
    const { name, activeIconName } = this.state
    const { navigation, colors } = this.props
    const icons = [
      {name: 'home'},
      {name: 'cafe'},
      {name: 'cloud'},
      {name: 'camera'},
      {name: 'calculator'},
      {name: 'clipboard'},
      {name: 'flash'},
      {name: 'planet'},
      {name: 'business'},
      {name: 'nuclear'},
      {name: 'rocket'},
      {name: 'bed'},
    ]
    return(
      <AddWorkspaceView>
        <ScrollView>
          <LinearGradient colors={[colors.from, colors.from, colors.from, colors.from, colors.to]} style={styles.linearGradient} />
          <Container>
            <EmailInput>
              <EmailText>Name</EmailText>
              <Email
                value = { this.state.name }
                maxLength = { 100 }
                color = "#FFF"
                fontSize = { 18 }
                selectionColor = "#FFF"
                returnKeyLabel = "next"
                autoCapitalize = "words"
                textContentType = "emailAddress"
                onChangeText = { name => this.setState({ name }) }/>
            </EmailInput>
            <IconsView>
              <Container>
                <IconsText>Choose Your Icon</IconsText>
                <Icons>
                  {
                    icons.map((icon,index) => (
                      <IconView key = { index } onPress = { () => this.setState({ activeIconName: icon.name, index }) } active = { this.state.index === index }><Ionicons name = {`ios-${icon.name}`} color = { colors.baseColor } size = {45} /></IconView>
                    ))
                  }
                </Icons>
              </Container>
            </IconsView>
          </Container>
        </ScrollView>
        <AddButton baseColor = { colors.baseColor } text = "Create new Workspace" onPress = { () => name !== '' && activeIconName !== '' ? this._createWorkspace(name) : null } />
      </AddWorkspaceView>
    )
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    width: '200%',
    height: 1000,
    position: 'absolute',
    left: '-50%',
    top: -730,
    zIndex: -999,
    transform: [{ rotate: '-15deg'}],
    shadowOpacity: 0.61,
    shadowRadius: 3,
    shadowColor: 'rgba(209,209,209,1)',
    resizeMode: 'cover'
  }
})

const mapStateToProps = state => {
  return {
    workspaces: state.workspaces,
    userId: state.user.data.user._id,
    colors: state.color.color
  }
}

const mapDispatchToProps = dispatch => {
	return {
		createWorkspace: (name,icon,userId) => dispatch(createWorkspace(name,icon,userId))
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(AddWorkspace)
