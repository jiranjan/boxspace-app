import React from 'react'
import {
  AsyncStorage,
  Button,
  View,
  StyleSheet,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  SafeAreaView
} from 'react-native'
import styled from 'styled-components/native'
import { SignUp } from './../actions/userActions'
import { connect } from 'react-redux'
import Ionicons from 'react-native-vector-icons/Ionicons'

const SingInView = styled.View`
  flex: 1;
  background: #4D62FC;
`

const Container = styled.View`
  width: 85%;
  height: 80%;
  margin: auto;
`

const SignInHeaderText = styled.Text`
  font-size: 48;
  font-weight: 700;
  color: #FFFFFF;
`

const EmailInput = styled.View`
  margin-top: 40;
`

const EmailText = styled.Text`
  font-size: 21;
  font-weight: 500;
  color: #FFFFFF;
`

const Email = styled.TextInput`
  borderBottomWidth: 2px;
  border-color: #FFFFFF;
  margin-top: 20;
  padding-bottom: 5;
  padding-left: 5;
`

const PasswordInput = styled.View`
  margin-top: 40;
`

const PasswordText = styled.Text`
  font-size: 21;
  font-weight: 500;
  color: #FFFFFF;
`

const Password = styled.TextInput`
  borderBottomWidth: 2px;
  border-color: #FFFFFF;
  margin-top: 20;
  padding-bottom: 5;
  padding-left: 5;
`

const SignInButton = styled.TouchableOpacity`
  width: 200;
  height: 52;
  border-radius: 26;
  background-color: #FFF;
  align-self: center;
  margin-top: 40;
  margin-bottom: 30;
`

const SignInText = styled.Text`
  font-size: 17;
  font-weight: 500;
  color: #000;
  margin: auto;
`

const SignUpText = styled.Text`
  font-size: 14;
  color: #FFF;
  font-weight: 400;
  position: absolute;
  bottom: 0;
  align-self: center;
`

class SignUpScreen extends React.Component {
  static navigationOptions = {
    headerStyle: {
      borderBottomWidth: 0,
      backgroundColor: '#4D62FC',
      elevation: null,
   },
   headerTintColor: '#FFF',
  };

  constructor(props){
    super(props)
    this.state = {
      showSignUp: false,
      email: '',
      password: '',
      name: ''
    }
  }

  componentDidUpdate(prevProps,prevState){
    if (prevProps.user !== this.props.user && this.props.user !== null) {
      this.props.navigation.navigate('App')
    }
  }

  render() {
    const { navigation } = this.props
    return (
      <SingInView>
        <SafeAreaView />
        <Container>
          <SignInHeaderText>Sign up</SignInHeaderText>
          <EmailInput>
            <EmailText>Name and surname</EmailText>
            <Email
              value = { this.state.name }
              maxLength = { 100 }
              color = "#FFF"
              fontSize = { 18 }
              selectionColor = "#FFF"
              returnKeyLabel = "next"
              autoCapitalize = "words"
              textContentType = "emailAddress"
              enablesReturnKeyAutomatically
              onChangeText = { name => this.setState({ name }) }/>
          </EmailInput>
          <EmailInput>
            <EmailText>Email</EmailText>
            <Email
              value = { this.state.email }
              maxLength = { 100 }
              color = "#FFF"
              fontSize = { 18 }
              autoCapitalize = "none"
              selectionColor = "#FFF"
              returnKeyLabel = "next"
              keyboardType = "email-address"
              textContentType = "name"
              enablesReturnKeyAutomatically
              onChangeText = { email => this.setState({ email }) }/>
          </EmailInput>
          <PasswordInput>
            <PasswordText>Password</PasswordText>
            <Password
              value = { this.state.password }
              maxLength = { 30 }
              color = "#FFF"
              selectionColor = "#FFF"
              fontSize = { 18 }
              secureTextEntry
              returnKeyLabel = "done"
              textContentType = "password"
              enablesReturnKeyAutomatically
              onChangeText = { password => this.setState({ password }) }/>
          </PasswordInput>
          <SignInButton onPress = { () => this._signInAsync() }>
            <SignInText>Sign up</SignInText>
          </SignInButton>
          <TouchableOpacity onPress = { () => navigation.navigate('SignUpScreen') }>
            <SignUpText>Terms of use.</SignUpText>
          </TouchableOpacity>
        </Container>
      </SingInView>
    );
  }

  _signInAsync = async () => {
    await this.props.signUp( this.state.name, this.state.email, this.state.password )
  };
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
	return {
		signUp: (name,email,password) => dispatch(SignUp(name,email,password)),
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen)
