import React from 'react'
import {
  StatusBar,
  StyleSheet,
  View,
  Text
} from 'react-native'
import { connect } from 'react-redux'
import Loader from './../components/Loader'
import { getCurrentUser } from './../actions/userActions'

import styled from 'styled-components/native'

const AuthLoadingView = styled.View`
  background-color: #fff;
  flex: 1;
`

const LoadingText = styled.Text`
  margin: auto;
  padding-top: 120;
`

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props)
    this._bootstrapAsync()
  }

  _bootstrapAsync = async () => {
    const user = await this.props.user.data

    this.props.navigation.navigate(user ? 'Home' : 'Auth')
  }

  render() {
    return (
      <AuthLoadingView>
        <StatusBar barStyle="default" />
        <Loader/>
        <LoadingText>Loading..</LoadingText>
      </AuthLoadingView>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
	return {
		getCurrentUser: () => dispatch(getCurrentUser())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen)
