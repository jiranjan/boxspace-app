import React from 'react'
import {
  AsyncStorage,
  Button,
  View,
  StyleSheet,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'
import { SignIn } from './../actions/userActions'
import { connect } from 'react-redux'
import Ionicons from 'react-native-vector-icons/Ionicons'

const SingInView = styled.View`
  flex: 1;
  background: #4D62FC;
`

const Container = styled.View`
  width: 85%;
  height: 80%;
  margin: auto;
`

const SignInHeaderText = styled.Text`
  font-size: 48;
  font-weight: 700;
  color: #FFFFFF;
`

const EmailInput = styled.View`
  margin-top: 40;
`

const EmailText = styled.Text`
  font-size: 21;
  font-weight: 500;
  color: #FFFFFF;
`

const Email = styled.TextInput`
  borderBottomWidth: 2px;
  border-color: ${ props => props.error ? "#FF1968" : "#FFFFFF" };
  margin-top: 20;
  padding-bottom: 5;
  padding-left: 5;
`

const PasswordInput = styled.View`
  margin-top: 40;
`

const PasswordText = styled.Text`
  font-size: 21;
  font-weight: 500;
  color: #FFFFFF;
`

const Password = styled.TextInput`
  borderBottomWidth: 2px;
  border-color: ${ props => props.error ? "#FF1968" : "#FFFFFF" };
  margin-top: 20;
  padding-bottom: 5;
  padding-left: 5;
`

const SignInButton = styled.TouchableOpacity`
  width: 200;
  height: 52;
  border-radius: 26;
  background-color: #FFF;
  align-self: center;
  margin-top: 40;
  margin-bottom: 30;
`

const SignInText = styled.Text`
  font-size: 17;
  font-weight: 500;
  color: #000;
  margin: auto;
`

const SignUpText = styled.Text`
  font-size: 14;
  color: #FFF;
  font-weight: 400;
  position: absolute;
  bottom: 0;
  align-self: center;
`

class SignInScreen extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      showSignUp: false,
      email: '',
      password: '',
      emailError: false,
      passwordError: false,
      showError: false,
      showSuccess: false
    }
  }

  static navigationOptions = {
    headerStyle: {
      borderBottomWidth: 0,
      backgroundColor: '#4D62FC',
      elevation: null,
     },
     header: null
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user.data !== null) {
      this.setState({ showError: false }, () => this.setState({ showSuccess: true }))
    }
    if (nextProps.user.error !== null) {
      this.setState({ showError: true })
    }
  }

  render() {
    const { navigation } = this.props
    return (
      <SingInView>
        <SafeAreaView />
        <Container>
          <SignInHeaderText>Sign in</SignInHeaderText>
          <EmailInput>
            <EmailText>Email</EmailText>
            <Email
              error = { this.state.emailError }
              value = { this.state.email }
              maxLength = { 100 }
              color = { this.state.emailError ? "#FF1968" : "#FFF" }
              fontSize = { 18 }
              selectionColor = "#FFF"
              returnKeyType = "next"
              autoCapitalize = "none"
              textContentType = "emailAddress"
              keyboardType = "email-address"
              enablesReturnKeyAutomatically
              onChangeText = { email => this.setState({ email, emailError: false }) }/>
          </EmailInput>
          <PasswordInput>
            <PasswordText>Password</PasswordText>
            <Password
              error = { this.state.passwordError }
              value = { this.state.password }
              maxLength = { 30 }
              color = "#FFF"
              selectionColor = "#FFF"
              fontSize = { 18 }
              secureTextEntry
              returnKeyType = "send"
              textContentType = "password"
              onSubmitEditing = { () => this._signInAsync() }
              enablesReturnKeyAutomatically
              onChangeText = { password => this.setState({ password, passwordError: false }) }/>
          </PasswordInput>
          <SignInButton onPress = { () => this._signInAsync() }>
            {
              this.state.showError ?
              <LottieView
                source = { require('./../animations/error.json') }
                autoPlay
                onAnimationFinish = { () => this.setState({ showError: false }) }
                loop = { false }/>
              :
              this.state.showSuccess ?
              <LottieView
                source = { require('./../animations/success.json') }
                autoPlay
                onAnimationFinish = { () => this.props.navigation.navigate('App') }
                loop = { false }/>
              :
              <SignInText>Sign in</SignInText>
            }
          </SignInButton>
          <TouchableOpacity onPress = { () => navigation.navigate('SignUpScreen') }>
            <SignUpText>Don't have an account? Create</SignUpText>
          </TouchableOpacity>
        </Container>
      </SingInView>
    );
  }

  _signInAsync = async () => {
    const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    if (emailRegex.test(this.state.email) && this.state.password !== '') {
      await this.props.signIn( this.state.email, this.state.password )
    }else if(this.state.password === '' && emailRegex.test(this.state.email) === false) {
      this.setState({ passwordError: true, emailError: true })
    }else if (this.state.password === '') {
      this.setState({ passwordError: true })
    }else {
      this.setState({ emailError: true })
    }
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
	return {
		signIn: (email,password) => dispatch(SignIn(email,password)),
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen)
