import { SIGN_IN_SUCCESS, SIGN_IN_PENDING, SIGN_IN_ERROR, SIGN_OUT } from './../actions/types'

const initialState = {
  isLoading: false,
  error: null,
  data: null
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SIGN_IN_PENDING:
      return Object.assign({}, state, {
        isLoading: true
      })

    case SIGN_IN_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      })

    case SIGN_IN_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        error: null,
        data: action.payload
      })

    case SIGN_OUT:
      return initialState;

    default:
      return state;
  }
}
