import { combineReducers } from 'redux'

import userActionsReducer from './userActionsReducer'
import workspaceActionsReducer from './workspaceActionsReducer'
import todoActionsReducer from './todoActionsReducer'
import noteActionsReducer from './noteActionsReducer'
import changeColorActionsReducer from './changeColorActionsReducer'

export default combineReducers({
  user: userActionsReducer,
  workspaces: workspaceActionsReducer,
  todos: todoActionsReducer,
  notes: noteActionsReducer,
  color: changeColorActionsReducer,
})
