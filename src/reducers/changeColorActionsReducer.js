import * as ActionTypes from './../actions/types'

const initialState = {
  color: {
    from: '#4D62FC',
    to: '#263AD3',
    name: 'blue',
    baseColor: '#263AD3'
  }
}

export default function(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.CHANGE_COLOR:
      return Object.assign({}, state, {
        color: action.payload
      })

    case ActionTypes.SIGN_OUT:
      return initialState;

    default:
      return state
  }
}
