import * as ActionTypes from './../actions/types'

const initialState = {
  isLoading: false,
  error: null,
  data: []
}

export default function(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.FETCH_NOTES_PENDING:
      return Object.assign({}, state, {
        isLoading: true
      })

    case ActionTypes.FETCH_NOTES_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      })

    case ActionTypes.FETCH_NOTES_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        error: null,
        data: action.payload
      })

    case ActionTypes.SIGN_OUT:
      return initialState;

    default:
      return state;
  }
}
