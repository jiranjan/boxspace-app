import React from 'react'
import Ionicons from 'react-native-vector-icons/Ionicons'
import HomeScreen from './screens/HomeScreen'
import SignInScreen from './screens/SignInScreen'
import SignUpScreen from './screens/SignUpScreen'
import AuthLoadingScreen from './screens/AuthLoadingScreen'
import AddWorkspace from './screens/AddWorkspace'
import DetailScreen from './screens/DetailScreen'
import SettingsScreen from './screens/SettingsScreen'
import ChangeColorScreen from './screens/ChangeColorScreen'

import { createSwitchNavigator, createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation'

const AppStack = createStackNavigator({
  Home: HomeScreen,
  SettingsScreen: SettingsScreen
},{
  mode: 'modal',
  headerMode: 'none',
})

const AuthStack = createStackNavigator({
  SignIn: SignInScreen,
  SignUpScreen
})

const AppDetailStack = createStackNavigator({
  App: {
    screen: AppStack,
    navigationOptions: ({ navigation }) => {
      return {
        headerStyle: {
          borderBottomWidth: 0,
          elevation: null,
          backgroundColor: navigation.state.routes[0].params && navigation.state.routes.length < 2 ? navigation.state.routes[0].params.color : '#FFF'
        },
        headerTintColor: '#FFF',
        header: null
      }
    }
  },
  AddWorkspace: AddWorkspace,
  DetailScreen: DetailScreen,
  ChangeColorScreen: ChangeColorScreen
})

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppDetailStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
))
