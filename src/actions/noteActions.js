import * as ActionTypes from './types'
import axios from 'axios'
import { store } from './../../store'
import { fetchWorkspaces } from './workspaceActions'
import { Sentry } from 'react-native-sentry'

const notesReceived = (data) => ({
  type: ActionTypes.FETCH_NOTES_SUCCESS,
  payload: data,
  status: 'success'
})

const notesActionPending = () => ({
  type: ActionTypes.FETCH_NOTES_PENDING
})

const notesActionError = error => ({
  type: ActionTypes.FETCH_NOTES_ERROR,
  status: 'error',
  error: error.response.data
})

export const addNote = ( name, content, workspace, user ) => dispatch => {
  axios.post(`http://207.154.211.81:1337/notes/`, {
    name,
    content,
    workspace,
    user
  },{
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => dispatch(fetchNotes(workspace)))
  .catch(error => {
    Sentry.captureException(err)
    dispatch(notesActionError(error))
  })
}

export const fetchNotes = id => dispatch => {
  dispatch(notesActionPending())
  axios.get(`http://207.154.211.81:1337/notes/`,{
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => {
    const data = res.data.filter(t => t.workspace._id === id)
    dispatch(notesReceived(data))
  })
  .catch(error => {
    Sentry.captureException(error)
    dispatch(notesActionError(error))
  })
}
