import * as ActionTypes from './types'
import axios from 'axios'
import { store } from './../../store'
import { fetchWorkspaces } from './workspaceActions'
import { Sentry } from 'react-native-sentry'

const todosReceived = (data) => ({
  type: ActionTypes.FETCH_TODOS_SUCCESS,
  payload: data,
  status: 'success'
})

const todoActionPending = () => ({
  type: ActionTypes.FETCH_TODOS_PENDING
})

const todoActionError = error => ({
  type: ActionTypes.FETCH_TODOS_ERROR,
  status: 'error',
  error: error.response.data
})

export const addTodo = ( name, done, workspace ) => dispatch => {
  axios.post(`http://207.154.211.81:1337/todos/`, {
    name,
    done,
    workspace
  },{
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => dispatch(fetchTodos(workspace)))
  .catch(error => {
    Sentry.captureException(error)
    dispatch(todoActionError(error))
  })
}

export const changeTodoState = (done,id,wID) => dispatch => {
  axios.put(`http://207.154.211.81:1337/todos/${id}`, {
    done
  },{
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => dispatch(fetchTodos(wID)))
  .catch(error => {
    Sentry.captureException(error)
    dispatch(todoActionError(error))
  })
}

export const fetchTodos = id => dispatch => {
  dispatch(todoActionPending())
  axios.get(`http://207.154.211.81:1337/todos/`,{
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => {
    const data = res.data.filter(t => t.workspace._id === id)
    dispatch(todosReceived(data))
  })
  .catch(error => {
    Sentry.captureException(error)
    dispatch(todoActionError(error))
  })
}
