import * as ActionTypes from './types'
import axios from 'axios'
import { store } from './../../store'
import { Sentry } from 'react-native-sentry'
var ReadImageData = require('NativeModules').ReadImageData

const userReceived = (data) => ({
  type: ActionTypes.SIGN_IN_SUCCESS,
  payload: data,
  status: 'success'
})

const userActionPending = () => ({
  type: ActionTypes.SIGN_IN_PENDING
})

const userActionError = error => ({
  type: ActionTypes.SIGN_IN_ERROR,
  status: 'error',
  error: error.response.data
})

export const SignOut = () => ({
  type: ActionTypes.SIGN_OUT
})

export const SignIn = ( email, password ) => dispatch => {
  dispatch(userActionPending())
  axios.post(`http://207.154.211.81:1337/auth/local/`, {
    identifier: email,
    password: password
  })
  .then(res => dispatch(userReceived(res.data)))
  .catch(error => {
    dispatch(userActionError(error))
  })
}

export const uploadAvatar = (data,id) => dispatch => {
  dispatch(userActionPending())
  let avatarData = new FormData()
  avatarData.append('files', {
    'filename': data.fileName,
    'uri': data.uri,
    'type': data.type,
    'name': 'avatar.jpg',
    'size': data.fileSize
  })
  avatarData.append('refId', id)
  avatarData.append('ref', 'User')
  avatarData.append('source', 'users-permissions')
  avatarData.append('field', 'avatar')
  //  "files": "...", // Buffer or stream of file(s)
  // "path": "user/avatar", // Uploading folder of file(s).
  // "refId": "5a993616b8e66660e8baf45c", // User's Id.
  // "ref": "user", // Model name.
  // "source": "users-permissions", // Plugin name.
  // "field": "avatar" // Field name in the User model.
  axios.post(`http://207.154.211.81:1337/upload/`, avatarData, {
    headers: {
      'Authorization': `Bearer ${store.getState().user.data.jwt}`,
      'Content-Type': 'multipart/form-data'
    }
  })
  .then(r => dispatch(getCurrentUser()))
  .catch(error => {
    Sentry.captureException(error)
    dispatch(userActionError(error))
  })
}

export const getCurrentUser = () => dispatch => {
  axios.get(`http://207.154.211.81:1337/users/me/`,{
    headers: {
      'Authorization': `Bearer ${store.getState().user.data.jwt}`,
    }
  })
  .then(res => {
    const data = {
      jwt: store.getState().user.data.jwt,
      user: res.data
    }
    dispatch(userReceived(data))
  })
  .catch(err => Sentry.captureException(error))
}

export const SignUp = ( username, email, password ) => dispatch => {
  axios.post(`http://207.154.211.81:1337/auth/local/register/`, {
    username,
    email,
    password
  })
  .then(res => {
    const data = res.data
    dispatch(userReceived(data))
  })
  .catch(error => {
    console.log(error);
  })
}
