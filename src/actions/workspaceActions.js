import * as ActionTypes from './types'
import axios from 'axios'
import { store } from './../../store'
import { Sentry } from 'react-native-sentry'

const workspaceReceived = data => ({
  type: ActionTypes.CREATE_WORKSPACE_SUCCESS,
  payload: data,
  status: 'success'
})

const workspaceActionPending = () => ({
  type: ActionTypes.CREATE_WORKSPACE_PENDING
})

const workspaceActionError = error => ({
  type: ActionTypes.CREATE_WORKSPACE_ERROR,
  status: 'error',
  error
})

export const fetchWorkspaces = () => dispatch => {
  dispatch(workspaceActionPending())
  axios.get(`http://207.154.211.81:1337/workspaces/`, {
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => {
    const data = res.data.filter(w => w.users.find(u => u._id === store.getState().user.data.user._id))
    dispatch(workspaceReceived(data))
  })
  .catch(error => {
    Sentry.captureException(error)
    dispatch(workspaceActionError(error))
  })
}

export const createWorkspace = (name,icon,users) => dispatch => {
  axios.post(`http://207.154.211.81:1337/workspaces/`, {
    name,
    icon,
    users
  },
  {
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => dispatch(fetchWorkspaces()))
  .catch(error => {
    Sentry.captureException(error)
    dispatch(workspaceActionError(error))
  })
}

export const updateWorkspace = (userEmail,allUsers, workspaceId) => dispatch => {
  dispatch(workspaceActionPending())
  axios.get(`http://207.154.211.81:1337/users/`, {
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => {
    const users = allUsers.map(a => a._id)
    const userId = res.data.filter(u => u.email === userEmail)[0]._id
    users.push(userId)
    axios.put(`http://207.154.211.81:1337/workspaces/${workspaceId}`, {
      users
    },
    {
      headers: {
      Authorization: `Bearer ${store.getState().user.data.jwt}`
      }
    })
    .then(res => dispatch(fetchWorkspaces()))
    .catch(error => {
      Sentry.captureException(error)
      dispatch(workspaceActionError(error))
    })
  })
  .catch(e => {
    dispatch(workspaceActionError(e))
  })
}

export const acceptInvite = id => dispatch => {
  dispatch(workspaceActionPending())
  axios.put(`http://207.154.211.81:1337/workspaces/${id}`, {
    shared: true
  },
  {
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .then(res => dispatch(fetchWorkspaces()))
  .catch(error => {
    Sentry.captureException(error)
    dispatch(workspaceActionError(error))
  })
}

export const addTodo = (todoName,workspaceId) => dispatch => {
  dispatch(workspaceActionPending())
  axios.post(`http://207.154.211.81:1337/todos/`, {
    name: todoName,
    done: false,
    workspace: workspaceId
  },
  {
    headers: {
    Authorization: `Bearer ${store.getState().user.data.jwt}`
    }
  })
  .catch(error => {
    Sentry.captureException(error)
    dispatch(workspaceActionError(error))
  })
}
