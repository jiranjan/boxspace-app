import * as ActionTypes from './types'

export const changeColor = (color) => ({
  type: ActionTypes.CHANGE_COLOR,
  payload: color
})
