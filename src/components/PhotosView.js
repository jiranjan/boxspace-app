import React, { Component } from 'react';
import {
  Image,
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  ScrollView
} from 'react-native'
import styled from 'styled-components/native'
import { connect } from 'react-redux'
import Ionicons from 'react-native-vector-icons/Ionicons'

import AddButton from './AddButton'

const Photo = styled.Image`
  width: 100%;
  padding-top: 100%
`

const PhotoTouchableView = styled.TouchableOpacity`
  width: 32%;
  margin: 2.5px;
  justify-content: center;
  align-items: center;
`

const List = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`

const CloseButton = styled.TouchableOpacity`
  margin-left: auto;
  margin-right: 10;
`

const CloseButtonText = styled.Text`
  font-size: 18;
  color: ${props => props.color}
  font-weight: 500;
`

const PhotosView = props =>  {
  const { color, onClose, photoArray, choosenPhoto } = props
  return (
    <ScrollView style = {{ flex: 1 }}>
      <SafeAreaView />
      <View style = {{ flexDirection: 'row', marginTop: 15, marginBottom: 15 }}>
        <CloseButton onPress = { () => onClose() }><CloseButtonText color = { color }>Close</CloseButtonText></CloseButton>
      </View>
      <List>
        {
          photoArray.map((a,i) => (
            <PhotoTouchableView
              key = { i }
              onPress={() => choosenPhoto(a.node.image.uri) }>
              <Photo
                source={{ uri: a.node.image.uri }} />
            </PhotoTouchableView>
          ))
        }
      </List>
    </ScrollView>
  )
}

export default PhotosView;
