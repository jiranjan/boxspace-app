import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'

const WorkspaceView = styled.TouchableOpacity`
  width: 49%;
  height: 190;
  background: #FFF;
  border-radius: 15;
  justify-content: center;
  align-items: center;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
  margin-top: 2%;
`

const BoxAddText = styled.Text`
  font-size: 24;
  font-weight: 700;
  color: #4B4B4B;
  text-align: center;
  max-width: 90%;
`

const WorkSpace = (props) => {
  const { name, onPress, baseColor, icon } = props
  return(
    <WorkspaceView onPress = { () => onPress(name) }>
      <Ionicons name = {`ios-${icon}`} color = { props.baseColor } size = { 60 } />
      <BoxAddText>{name}</BoxAddText>
    </WorkspaceView>
  )
}

export default WorkSpace
