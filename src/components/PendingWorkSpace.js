import React from 'react'
import {
  View,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'

const WorkspaceView = styled.View`
  width: 100%;
  height: 60;
  background: ${ props => props.color };
  shadow-opacity: 0.11;
  border-radius: 5;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
  position: absolute;
  bottom: -70;
  z-index: 999;
  flex-direction: row;
  align-items: center;
`

const DismissIcon = styled.TouchableOpacity`
  margin-left: 15;
  margin-right: 15;
`

const InfoTabView = styled.View`

`

const InfoTabHeading = styled.Text`
  font-size: 14;
  color: #FFFFFF;
  font-weight: 500;
  margin-bottom: 5;
`

const InfoTabText = styled.Text`
  font-size: 12;
  color: #D1D1D1;
  font-weight: 500;
`

const AcceptButtonText = styled.Text`
  font-size: 14;
  color: #FFFFFF;
  font-weight: 500;
`

const PendingWorkSpace = (props) => {
  const { workspace, baseColor, onAcceptPress, ownUser} = props
  const user = workspace.users.filter(u => u._id !== ownUser._id)[0]
  return(
    <WorkspaceView color = { baseColor }>
      <DismissIcon>
        <Ionicons name = 'ios-close-circle' color = '#FFF' size = { 30 }  />
      </DismissIcon>
      <InfoTabView>
        <InfoTabHeading>{user.email}</InfoTabHeading>
        <InfoTabText>{workspace.name}</InfoTabText>
      </InfoTabView>
      <TouchableOpacity onPress = { () => onAcceptPress() } style = {{ marginLeft: 'auto', marginRight: 15 }}><AcceptButtonText>Accept</AcceptButtonText></TouchableOpacity>
    </WorkspaceView>
  )
}

export default PendingWorkSpace
