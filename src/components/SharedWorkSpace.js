import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { CachedImage } from 'react-native-cached-image'

import TestImage from './../images/profile-test.jpg'

const WorkspaceView = styled.TouchableOpacity`
  width: 49%;
  height: 190;
  background: #FFFFFF;
  border-radius: 15;
  justify-content: center;
  align-items: center;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
  margin-top: 2%;
`

const PendingLabel = styled.TouchableOpacity`
  width: 30;
  height: 30;
  border-radius: 15;
  align-items: center;
  align-self: center;
  position: absolute;
  left: 10;
  top: 10;
  overflow: hidden;
`

const PendingLabelText = styled.Text`
  color: #FFFFFF;
  font-size: 12;
  font-weight: 700;
`

const BoxAddText = styled.Text`
  font-size: 24;
  font-weight: 700;
  color: #4B4B4B;
  text-align: center;
  max-width: 90%;
`

const SharedWorkSpace = (props) => {
  const { workspace, baseColor, onPress, ownUser, shared } = props
  const user = workspace.users.filter(u => u._id !== ownUser._id)[0]
  let imageUri;
  if (user.avatar) {
    imageUri = { uri: `http://207.154.211.81:1337${user.avatar.url}` }
  }else {
    imageUri = TestImage
  }
  return(
    <WorkspaceView onPress = { () => onPress() }>
      {
        shared &&
        <PendingLabel>
          <CachedImage style = {{ width: 30, height: 30 }} source = {imageUri} />
        </PendingLabel>
      }
      <Ionicons name = {`ios-${workspace.icon}`} color = { baseColor } size = { 60 } />
      <BoxAddText>{workspace.name}</BoxAddText>
    </WorkspaceView>
  )
}

export default SharedWorkSpace
