import React, { Component, Fragment } from 'react'
import { View, Text, FlatList, Image, TouchableOpacity, AsyncStorage, Animated, Easing } from 'react-native'
import AnimatedModal from 'react-native-modal'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Swipeout from 'react-native-swipeout'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'

import LogoIcon from './../images/ICON.png'

const NotFinishedIcon = styled.TouchableOpacity`
  width: 23;
  height: 23;
  border-radius: 12;
  border-width: 2px;
  border-color: #C3C3C3;
  align-self: center;
  padding: 10px;
`

const TodoIcon = styled.Image`
  width: 23;
  height: 23;
  align-self: center;
  padding: 10px;
`

class Checker extends Component{
  constructor(props){
    super(props)
    this.state = { checked: this.props.checked, progress: this.props.checked ? new Animated.Value(1) : new Animated.Value(0) }
  }

  changeState = async () => {
    await this.setState({ checked: !this.state.checked })
    await this.props.changeTodoState(this.state.checked)
    if (this.props.checked) {
      Animated.timing(this.state.progress, {
        toValue: 0,
        duration: 1000,
        easing: Easing.linear,
      }).start()
    }else {
      Animated.timing(this.state.progress, {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear,
      }).start()
    }
  }

  render(){
    return(
      <Fragment>
        <TouchableOpacity style = {{ alignSelf: 'center', width: 80, height: 80, marginRight: -40 }} onPress = { () => this.changeState() }>
          <LottieView
            source = { require('./../animations/done.json') }
            onAnimationFinish = { () => this.setState({ showError: false }) }
            ref = { animation => {
              this.animation = animation
            }}
            loop = { false }
            progress = { this.state.progress }/>
        </TouchableOpacity>
      </Fragment>
    )
  }
}

export default Checker
