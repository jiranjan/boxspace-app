import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import LottieView from 'lottie-react-native'

const Loader = (props) => {
  return(
    <LottieView
      source = { require('./../animations/ice_cream_animation.json') }
      autoPlay
      loop />
  )
}

export default Loader
