import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Ionicons from 'react-native-vector-icons/Ionicons'

const ButtonView = styled.TouchableOpacity`
  width: 200;
  height: 52;
  background: ${ props => props.baseColor };
  border-radius: 26;
  z-index: 999;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 0;
  margin-bottom: 20;
  align-self: center;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
`

const ButtonText = styled.Text`
  font-size: 14;
  font-weight: 500;
  color: #fff;
`

const AddButton = (props) => {
  const { text, onPress, baseColor } = props
  return(
    <ButtonView baseColor = { baseColor } onPress = {() => onPress()}>
      <ButtonText>{text}</ButtonText>
    </ButtonView>
  )
}

export default AddButton
