import React, { Component } from 'react'
import { View, Text, ScrollView, AsyncStorage } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import styled from 'styled-components/native'

const NotesView = styled.View`
  border-radius: 18;
  width: 85%;
  margin: auto;
  margin-top: 7;
`

const NoteView = styled.View`
  background: #FFF;
  border-radius: 18;
  width: 100%;
  margin: auto;
  margin-top: 15;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
`

const Container = styled.View`
  width: 80%;
  padding: 40px 0;
  margin: auto;
`

const NoteHeading = styled.View`
  flex-direction: row;
  margin-top: -10;
`

const NoteHeadingText = styled.Text`
  font-size: 21;
  font-weight: 500;
  max-width: 70%;
  margin-top: -8;
`

const NoteLeadingText = styled.Text`
  font-size: 10;
  color: #A3A3A3;
  margin-bottom: 3;
  margin-left: auto;
`

const NoteDescription = styled.Text`
  font-size: 14;
  color: #000;
  margin-top: 10;
`

const Notes = props => (
  <NotesView>
    {
      props.notes.map((n,i) => (
        <NoteView key = {i}>
          <Container>
            <NoteHeading>
              <NoteHeadingText>{ n.name }</NoteHeadingText>
              <NoteLeadingText>{ n.user }</NoteLeadingText>
            </NoteHeading>
            <NoteDescription numberOfLines = { 7 }>{ n.content }</NoteDescription>
          </Container>
        </NoteView>
      ))
    }
  </NotesView>
)

export default Notes
