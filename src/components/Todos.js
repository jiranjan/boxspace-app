import React, { Component } from 'react'
import { View, Text, FlatList, Image, TouchableOpacity, AsyncStorage } from 'react-native'
import AnimatedModal from 'react-native-modal'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Swipeout from 'react-native-swipeout'
import styled from 'styled-components/native'

import AddButton from './AddButton'
import Checker from './Checker'

const TodosView = styled.View`
  background: #FFF;
  height: 100%;
  border-radius: 18;
  width: 85%;
  margin: auto;
  margin-top: 7;
  shadow-opacity: 0.11;
  shadow-radius: 15;
  shadow-color: #000000;
  shadow-offset: 0px 0px;
`

const Container = styled.View`
  width: 80%;
  margin: 20px auto;
  padding-bottom: 100;
`
const Topbar = styled.View`
  flex-direction: row;
`

const CompletedNumText = styled.Text`
  font-size: 24;
  font-weight: 400;
  align-self: center;
  margin-bottom: -5;
`

const CompletedNumOfText = styled.Text`
  font-size: 38;
  font-weight: 600;
  align-self: flex-start;
`

const TodosHeading = styled.Text`
  font-size: 21;
  font-weight: 400;
  align-self: flex-end;
  margin-bottom: 4;
  margin-left: 5;
`

const TodoList = styled.FlatList`
  margin-top: 20;
`

const TodoView = styled.View`
  width: 100%;
  height: 50;
  flex-direction: row;
  justify-content: space-between;
`

const TodoText = styled.Text`
  font-size: 14;
  color: ${props => props.done ? '#B4B4B4' : '#000000'};
  align-self: center;
`

const AddRecipeView = styled.View`
  width: 100%;
  height: 230;
  background: #fff;
  margin: auto;
  border-radius: 15;
`

const AddRecipeHeadingText = styled.Text`
  font-size: 29;
  font-weight: 600;
  margin: 30px auto;
`

const AddRecipeInput = styled.TextInput`
  height: 30;
  width: 80%;
  border-bottom-width: 2px;
  border-color: #263AD3;
  padding-left: 5;
  margin: 0 auto;
`

const BottombarView = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  position: absolute;
  bottom: 20;
  width: 100%;
  margin: 0 auto;
`

const BottombarButtonText = styled.Text`
  font-size: 17;
  font-weight: 500;
`

const BottombarButtonTouchable = styled.TouchableOpacity`
  align-self: center;
  padding: 15px;
`

const CenterSpacer = styled.View`
  height: 45;
  width: 1;
  background: #707070;
`

var swipeoutBtns = [
  {
    text: 'Delete',
    backgroundColor: 'red',
    underlayColor: '#FFF',
    onPress: () => {  }
  }
]

const Todos = (props) => (
  <TodosView>
    <Container>
      <Topbar>
        <CompletedNumText style = {{ alignSelf: 'center', marginBottom: -8 }}>{props.todos.filter(t => t.done === true).length}</CompletedNumText>
        <CompletedNumText>/</CompletedNumText>
        <CompletedNumOfText>{props.todos.length}</CompletedNumOfText>
        <TodosHeading>Todo's</TodosHeading>
      </Topbar>
      {
        props.todos.map((t,i) => (
          <TodoView key = {i}>
            <TodoText done = {t.done}>{t.name}</TodoText>
            <Checker changeTodoState = { state => props.changeTodoState(state, t._id, t.workspace._id) } checked = { t.done } />
          </TodoView>
        ))
      }
    </Container>
  </TodosView>
)

export default Todos
