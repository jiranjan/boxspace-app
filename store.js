import {
  createStore,
  applyMiddleware,
  compose
} from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { composeWithDevTools } from 'redux-devtools-extension'
import createRavenMiddleware from 'raven-for-redux'
// import { setupSentry } from './sentry-utils'
// import { Sentry } from 'react-native-sentry'
//
// setupSentry()

import thunk from 'redux-thunk'

import rootReducer from './src/reducers'

const initialState = {};

// const stateTransformer = state => {
//   let maskedAuth
//   if (state.user.data) {
//     maskedAuth = masked(state.user.data.user, [
//       'email',
//       'username',
//       'lastName',
//       'id',
//       '_id'
//     ])
//   }
//   return { ...state, user: maskedAuth }
// }

const persistConfig = {
  key: 'root',
  storage,
}

const middleware = [thunk]

export const store = createStore(
  persistReducer(persistConfig, rootReducer),
  initialState,
  composeWithDevTools(
    applyMiddleware(
      ...middleware,
      // createRavenMiddleware(Sentry, {
      //   stateTransformer
      // }),
    )
  )
)

export const persistor = persistStore(store);
