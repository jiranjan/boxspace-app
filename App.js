import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { store, persistor } from './store'
import { PersistGate } from 'redux-persist/integration/react'
import i18n from './i18n.js'
import { I18nextProvider } from 'react-i18next'

import Router from './src/Router'
import Loader from './src/components/Loader'

global.PaymentRequest = require('react-native-payments').PaymentRequest;

import { Sentry } from 'react-native-sentry';

Sentry.config('https://f73733032c7b4d898be060aaaa35a660@sentry.io/1395397').install();


export default class App extends Component {
  render() {    
    return (
      <Provider store = { store }>
        <PersistGate persistor = { persistor }>
          <I18nextProvider i18n={ i18n }>
            <Router />
          </I18nextProvider>
        </PersistGate>
    	</Provider>
    )
  }
}
