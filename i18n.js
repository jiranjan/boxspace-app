import i18n from "i18next";
import { store } from './store';

export const languages = ["cs", "en"]

i18n.init({
	resources: {
		cs: {
			HomeScreen: {
				"I18N": "I18N překlady",
			}
		},
		en: {
			HomeScreen: {
				"I18N": "I18N translations",
			}
		},
	},
	fallbackLng: store.getState()['lang'],

	keySeparator: false,

	interpolation: {
	escapeValue: false,
	formatSeparator: ","

	},

	react: {
		wait: true
	},
});

export default i18n;
