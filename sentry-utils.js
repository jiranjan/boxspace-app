import { Sentry } from 'react-native-sentry'
import DeviceInfo from 'react-native-device-info'

let dsn = 'https://f73733032c7b4d898be060aaaa35a660@sentry.io/1395397'

export function setupSentry() {
  Sentry.config(dsn).install()
  addBuildContext()
}

const addBuildContext = () => {
  Sentry.setTagsContext({
    appVersion: DeviceInfo.getVersion(),
    buildNumber: DeviceInfo.getBuildNumber(),
    deviceInfo: {
      systemName: DeviceInfo.getSystemName(),
      systemVersion: DeviceInfo.getSystemVersion(),
      deviceName: DeviceInfo.getDeviceName()
    }
  })
}
