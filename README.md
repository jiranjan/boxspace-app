# React native boilerplate 🍽
Simple react native boilerplate setup for iOS/Android applications.
##### [Complete React native guidles](https://wiki.thinkeasy.cz/doku.php/howtos:react_native)

### 📖 List of main packages

1. Styled components
2. Redux, Redux thunk & Redux persist
3. React Navigation
4. Moment
5. I18next
6. RN Vector Icons
7. Lottie
8. RN config ( ENV BUILDS )
9. Axios

### 🚀 Getting started
1. `git clone`
2. `npm install`
3. `react-native run-ios/android` or xCode/Android studio startup.
4. ###### Additional
    * `.env` file contains urls for prodcution & development builds.
    * You can access this configurations by importing Config package as `import Config from 'react-native-config'` and use it like `Config.API_URL_DEVELOPMENT`.